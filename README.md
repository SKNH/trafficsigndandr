# README #

This is the pure Java project (driven by gradle) which allows to detect and recognize (DandR) road traffic signs.

Currently only speed limit group is implemented and it is still under testing.

This project uses **JavaOCR** library (http://sourceforge.net/projects/javaocr) which is slightly modified in order to eliminate java awt package dependencies and focus only on digits recognition.
This library located at **ocr** module. Compiled *jar* file located at **ocr/build/libs** directory.

Another library which is used in the main project is **detection** one. This library provides functionality to detect ellipse or triangle shapes at the given image. This library located at **detection** module. Compiled *jar* file located at **detection/build/libs** directory.

The main project itself is a real working example of how to use these libraries in plain Java project (and it still in progress up to now).

There is another example of the application that use these libraries, in this case - Android application:[Driver Assistant](https://bitbucket.org/ChernyshovYuriy/driver-assistant) 

### What is this repository for? ###

* This repository is created in purpose of sharing personal experience with objects detection and recognition in the field of road traffic signs.