/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.recognition.shape;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/4/14
 * Time: 8:57 PM
 */

import process.detection.matching_squares.Direction;
import process.detection.matching_squares.Path;
import process.recognition.ImageRecognizingCallback;
import process.recognition.RecognizedShape;

/**
 * This class defines whether the point of the shape is belongs to ellipse by using <b>Canonical form</b>
 * http://en.wikipedia.org/wiki/Ellipse#Canonical_form
 */
public class EllipseRecognition extends BaseRecognition implements ShapeRecognizerListener {

    /**
     * Delta value of the deviation near the 1 (see <b>Canonical form</b>)
     */
    private static final double DELTA = 0.2;

    /**
     * Radius of the ellipse in X axis
     */
    private int a;

    /**
     * Radius of the ellipse in Y axis
     */
    private int b;

    @Override
    public final void recognizeShape(final Path path, final ImageRecognizingCallback imageProcessingCallback) {

        Direction direction;
        int lastX = path.getOriginX() - path.getMinX();
        int lastY = path.getShapeHeight();

        init();

        a = path.getShapeWidth() >> 1;
        b = path.getShapeHeight() >> 1;

        processPoint(lastX, lastY);

        for (int i = 0; i < path.getLength(); i++) {

            direction = path.getDirections().get(i);

            lastX += direction.screenX;
            lastY -= direction.screenY;

            if (i % BASE_PROCESS_PERIOD == 0) {
                processPoint(lastX, lastY);
            }
        }

        if (getResult()) {
            final RecognizedShape recognizedShape = new RecognizedShape(RecognizedShape.SHAPE.ELLIPSE);
            recognizedShape.setMinX(path.getMinX());
            recognizedShape.setMaxX(path.getMaxX());
            recognizedShape.setMinY(path.getMinY());
            recognizedShape.setMaxY(path.getMaxY());
            recognizedShape.setPath(path);
            imageProcessingCallback.onRecognize(recognizedShape);
        }
    }

    /**
     * Process a single point of the shape
     * @param x X coordinate of the point
     * @param y Y coordinate of the point
     */
    private void processPoint(final int x, final int y) {
        int xTmp = x - a;
        int yTmp = y - b;
        double result = ((double) (xTmp * xTmp) / (double) (a * a)) + ((double) (yTmp * yTmp) / (double) (b * b));

        incrementPointsNumber();
        if (result < 1 - DELTA || result > 1 + DELTA) {
            incrementFailCounter();
        }
    }
}
