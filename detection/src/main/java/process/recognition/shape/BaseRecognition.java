/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.recognition.shape;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/6/14
 * Time: 5:06 PM
 */

/**
 * This is a base class for the different shapes recognition implementations, such as Triangle, Ellipse, etc ...
 */
public abstract class BaseRecognition {

    /**
     * Period which specifies a step to process data
     */
    protected static final int BASE_PROCESS_PERIOD = 10;

    /**
     * Value of the points which are out of the success result, in percentage
     */
    private static final int FAIL_PERCENTAGE_THRESHOLD = 20;

    /**
     * Overall points number
     */
    private int pointsNumber;

    /**
     * Counter of the failed points
     */
    private int failCounter;

    /**
     * This method return a result of the points processing.
     *
     * @return <b>true</b> if the shape meet triangle criteria, <b>false</b> otherwise
     */
    protected final boolean getResult() {
        //AppLogger.printMessage(this.getClass().getSimpleName() + " fail counter:" + failCounter +
        //        " Total counter:" + pointsNumber +
        //        " Fail threshold:" + ((pointsNumber * FAIL_PERCENTAGE_THRESHOLD) / 100));
        final int pointThreshold = 4;
        final int hundredPercent = 100;
        return pointsNumber > pointThreshold
                && failCounter <= ((pointsNumber * FAIL_PERCENTAGE_THRESHOLD) / hundredPercent);
    }

    /**
     * Initialize algorithm
     */
    protected final void init() {
        pointsNumber = 0;
        failCounter = 0;
    }

    /**
     * This method increment by one a points counter
     */
    protected final void incrementPointsNumber() {
        pointsNumber++;
    }

    /**
     * This method increment by one fails counter
     */
    protected final void incrementFailCounter() {
        failCounter++;
    }
}