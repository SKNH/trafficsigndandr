/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.recognition;

import process.detection.matching_squares.Path;
import process.recognition.shape.ShapeRecognizerListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/5/14
 * Time: 8:19 AM
 */
public class CompositeShapeRecognizer implements ShapeRecognizerListener {

    private final List<ShapeRecognizerListener> shapeRecognizerListeners = new ArrayList<ShapeRecognizerListener>();

    public CompositeShapeRecognizer(ShapeRecognizerListener... listeners) {
        Collections.addAll(shapeRecognizerListeners, listeners);
    }

    @Override
    public void recognizeShape(Path path, ImageRecognizingCallback imageRecognizingCallback) {
        for (ShapeRecognizerListener listener : shapeRecognizerListeners) {
            listener.recognizeShape(path, imageRecognizingCallback);
        }
    }
}