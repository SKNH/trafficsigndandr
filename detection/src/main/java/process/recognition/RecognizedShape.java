/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.recognition;

import process.detection.matching_squares.Path;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/7/14
 * Time: 8:59 PM
 */
public class RecognizedShape {

    public static enum SHAPE {
        TRIANGLE,
        ELLIPSE,
        UNRECOGNIZED
    }

    private Path path;

    private final SHAPE shape;

    private int minX;
    private int minY;
    private int maxX;
    private int maxY;

    public RecognizedShape(SHAPE shape) {
        this.shape = shape;
    }

    public SHAPE getShape() {
        return shape;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public int getMinX() {
        return minX;
    }

    public void setMinX(int minX) {
        this.minX = minX;
    }

    public int getMinY() {
        return minY;
    }

    public void setMinY(int minY) {
        this.minY = minY;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }
}