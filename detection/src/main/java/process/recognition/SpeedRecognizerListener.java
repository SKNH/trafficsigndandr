/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.recognition;

import ocr.ocrPlugins.mseOCR.OCRScanner;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/9/14
 * Time: 10:27 PM
 */
public interface SpeedRecognizerListener {

    /**
     * Call this method when need to recognize a speed number from the detected ellipse
     *
     * @param recognizedPixels recognized pixels
     * @param recognizedHeight recognized Height
     * @param recognizedWidth  recognized Width
     * @param ocrScanner       {@link ocr.ocrPlugins.mseOCR.OCRScanner}
     * @param callback         callback function
     */
    public void recognizeShape(OCRScanner ocrScanner, int[] recognizedPixels, int recognizedWidth,
                               int recognizedHeight, SpeedRecognition.ISpeedRecognitionCallback callback);
}