/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.recognition;

import ocr.ocrPlugins.mseOCR.OCRScanner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/10/14
 * Time: 7:34 AM
 */

/**
 * This class response for the recognition of the speed value of the detected speed limit sign.
 */
public class SpeedRecognition implements SpeedRecognizerListener {

    /**
     * Threshold value for the color component in order to eliminate "noise" pixels.
     */
    private static final int GRAY_THRESHOLD = 155;
    
    private static final int WHITE_COLOR = 0xFFFFFFFF;

    private static final int BYTE_MASK = 0xFF;

    //private final RescaleOp rescaleOp = new RescaleOp(1.5f, 15, null);

    /**
     * {@link process.recognition.SpeedRecognition.ISpeedRecognitionCallback} is an interface to provide callback
     * when recognition is complete.
     */
    public interface ISpeedRecognitionCallback {

        /**
         * On complete event.
         *
         * @param message Recognized value.
         */
        public void onComplete(final String message);
    }

    @Override
    public void recognizeShape(final OCRScanner ocrScanner,
                               final int[] recognizedPixels,
                               final int recognizedWidth,
                               final int recognizedHeight,
                               final ISpeedRecognitionCallback callback) {

        final int marginY = (recognizedHeight * 31) / 100;
        final int marginX = (recognizedWidth * 15) / 100;

        final BufferedImage image = new BufferedImage(recognizedWidth, recognizedHeight, BufferedImage.TYPE_BYTE_GRAY);
        for (int i = 0; i < recognizedWidth; i++) {
            for (int j = 0; j < recognizedHeight; j++) {
                image.setRGB(i, j, WHITE_COLOR);
            }
        }

        int x = marginX, y = marginY;

        for (int recognizedPixel : recognizedPixels) {
            int searchR, searchG, searchB;
            recognizedPixel = recognizedPixels[x + y * recognizedWidth];

            x++;
            if (x == recognizedWidth - marginX) {
                x = marginX;
                y++;
                if (y > recognizedHeight - marginY) {
                    break;
                }
            }

            searchR = (recognizedPixel >> 16) & BYTE_MASK; //red
            if (searchR > GRAY_THRESHOLD) {
                continue;
            }
            searchG = (recognizedPixel >> 8) & BYTE_MASK;  //green
            if (searchG > GRAY_THRESHOLD) {
                continue;
            }
            searchB = recognizedPixel & BYTE_MASK;         //blue
            if (searchB > GRAY_THRESHOLD) {
                continue;
            }

            image.setRGB(x, y, 0);
        }

        x = marginX;
        y = marginY;

        // Clear left and right edges lines from noise

        for (int i = 0; i < recognizedHeight; i++) {
            image.setRGB(x, i, WHITE_COLOR);
            image.setRGB(x + 1, i, WHITE_COLOR);
        }

        // Clear right top corner from noise

        x = recognizedWidth - marginX - 3;

        image.setRGB(x, y, WHITE_COLOR);
        image.setRGB(x + 1, y, WHITE_COLOR);
        image.setRGB(x + 2, y, WHITE_COLOR);
        image.setRGB(x + 3, y, WHITE_COLOR);
        image.setRGB(x, y + 1, WHITE_COLOR);
        image.setRGB(x + 1, y + 1, WHITE_COLOR);
        image.setRGB(x + 2, y + 1, WHITE_COLOR);
        image.setRGB(x + 3, y + 1, WHITE_COLOR);

        // Clear left top corner from noise

        x = marginX;

        image.setRGB(x, y, WHITE_COLOR);
        image.setRGB(x + 1, y, WHITE_COLOR);
        image.setRGB(x + 2, y, WHITE_COLOR);
        image.setRGB(x + 3, y, WHITE_COLOR);
        image.setRGB(x, y + 1, WHITE_COLOR);
        image.setRGB(x + 1, y + 1, WHITE_COLOR);
        image.setRGB(x + 2, y + 1, WHITE_COLOR);
        image.setRGB(x + 3, y + 1, WHITE_COLOR);

        // Clear left bottom corner from noise

        y = recognizedHeight - marginY * 2 - 1 + marginY;

        image.setRGB(x, y, WHITE_COLOR);
        image.setRGB(x + 1, y, WHITE_COLOR);
        image.setRGB(x + 2, y, WHITE_COLOR);
        image.setRGB(x + 3, y, WHITE_COLOR);
        image.setRGB(x, y + 1, WHITE_COLOR);
        image.setRGB(x + 1, y + 1, WHITE_COLOR);
        image.setRGB(x + 2, y + 1, WHITE_COLOR);
        image.setRGB(x + 3, y + 1, WHITE_COLOR);

        // Clear right bottom corner from noise

        x = recognizedWidth - marginX - 3;

        image.setRGB(x, y, WHITE_COLOR);
        image.setRGB(x + 1, y, WHITE_COLOR);
        image.setRGB(x + 2, y, WHITE_COLOR);
        image.setRGB(x + 3, y, WHITE_COLOR);
        image.setRGB(x, y + 1, WHITE_COLOR);
        image.setRGB(x + 1, y + 1, WHITE_COLOR);
        image.setRGB(x + 2, y + 1, WHITE_COLOR);
        image.setRGB(x + 3, y + 1, WHITE_COLOR);

        //rescaleOp.filter(image, image);

        /*ocrScanner.acceptAccuracyListener(new AccuracyListener() {
            @Override
            public void processCharOrSpace(OCRIdentification identAccuracy) {
                //AppLogger.printMessage("listener:" + identAccuracy);
            }
        });*/

        final int[] pixelsArray = new int[recognizedWidth * recognizedHeight];
        image.getRGB(0, 0, recognizedWidth, recognizedHeight, pixelsArray, 0, recognizedWidth);

        final File outputFile = new File("output/sub_image_to_scan.jpg");
        try {
            ImageIO.write(image, "jpg", outputFile);
        } catch (IOException e) {
            /*Ignore */
        }

        final String text = ocrScanner.scan(pixelsArray, recognizedWidth, recognizedHeight, 0, 0, 0, 0, null);

        callback.onComplete(text.trim());
    }
}
