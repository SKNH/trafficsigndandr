/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.detection.ihls_nhs;

import process.image.ImageDataStructure;
import process.image.ImageVertex;

/**
 * Created at Intellij IDEA
 * Author Yuri Chernyshov
 * on 25.05.14
 * at 17:13
 */

/**
 * {@link process.detection.ihls_nhs.RGBtoNHS} is a class that provides functionality to convert from
 * RGB color space into NHS color space.
 */
public class RGBtoNHS {

    /**
     * Convert RGB data into NHS data.
     * Use provided data structure in order to reuse it over processing loop.
     *
     * @param data               RGB data as bytes array.
     * @param imageDataStructure Instance of the {@link process.image.ImageDataStructure}.
     */
    public void convert(final int[] data, final ImageDataStructure imageDataStructure) {
        int sourceImageR;
        int sourceImageG;
        int sourceImageB;
        int saturation;
        int hue;
        int luminance = 0;
        int nhsBlue = 0;
        int nhsRed;
        int counter = 0;
        int x = 0;
        int y = 0;
        ImageVertex imageVertex;

        final IHLS_NHS ihls_nhs = new IHLS_NHS();
        for (int aData : data) {

            sourceImageR = (aData >> 16) & 0xff;
            sourceImageG = (aData >> 8) & 0xff;
            sourceImageB = (aData) & 0xff;

            hue = (int) ihls_nhs.retrieveNormalisedHue(sourceImageR, sourceImageG, sourceImageB);
            saturation = ihls_nhs.retrieveSaturation(sourceImageR, sourceImageG, sourceImageB);
            //luminance = (int) ihls_nhs.retrieveLuminance(sourceImageR, sourceImageG, sourceImageB);

            // Blue
            //nhsBlue = ihls_nhs.getNHSBlue(hue, saturation);

            // Red
            nhsRed = ihls_nhs.getNHSRed(hue, saturation);

            //nhsBlue = (0xff << 24) + (nhsBlue << 16) + (nhsBlue << 8) + nhsBlue;

            /*x++;
            if (x % width == 0) {
                x = 0;
                y++;
            }*/

            imageVertex = imageDataStructure.getVertexAt(counter++);
            imageVertex.setxPosition(x);
            imageVertex.setyPosition(y);
            imageVertex.setRedColor(sourceImageR);
            imageVertex.setGreenColor(sourceImageG);
            imageVertex.setBlueColor(sourceImageB);
            imageVertex.setHue(hue);
            imageVertex.setSaturation(saturation);
            imageVertex.setLuminance(luminance);
            imageVertex.setNhsBlue(nhsBlue);
            imageVertex.setNhsRed(nhsRed);
        }
    }
}
