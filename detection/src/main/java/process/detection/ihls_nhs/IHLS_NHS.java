/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.detection.ihls_nhs;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */

import utils.DefineConstants;
import utils.TabularDictionary;

/**
 * {@link process.detection.ihls_nhs.IHLS_NHS} is a class that process ARGB color space
 * in order to improve Hue Saturation Luminance and then to normalize Hue Saturation.
 */
public class IHLS_NHS {

    /**
     * Constant which holds a value of multiplication PI and 2
     */
    private static final double PI_MULTIPLY_BY_TWO = 6.283185307179586;

    /**
     * Calculating the hue value based on the blow formula:
     * <p/>
     * H = θ if B <= G
     * H = 2 * pi − θ if B > G
     *
     * @param red   Red color component.
     * @param green Green color component.
     * @param blue  Blue color component.
     *
     * @return The return value is normalised between 0 to 255.
     */
    public double retrieveNormalisedHue(final int red, final int green, final int blue) {
        double hue;
        if (blue <= green) {
            hue = retrieveTheta(red, green, blue);
        } else {
            hue = /*(2 * Math.PI)*/ PI_MULTIPLY_BY_TWO - retrieveTheta(red, green, blue);
        }
        return hue * 255 / /*(2 * Math.PI)*/ PI_MULTIPLY_BY_TWO;
    }

    /**
     * Luminance is calculated as:
     *
     * L = 0.210R + 0.715G + 0.072B
     *
     * @param red   Red color component.
     * @param green Green color component.
     * @param blue  Blue color component.
     *
     * @return Luminance value.
     */
    public double retrieveLuminance(final int red, final int green, final int blue) {
        return (0.210f * red) + (0.715f * green) + (0.072f * blue);
    }

    /**
     * Saturation is calculates as below:
     * <p/>
     * S = max(R, G, B) − min(R, G, B)
     *
     * @param red   Red color component.
     * @param green Green color component.
     * @param blue  Blue color component.
     *
     * @return Saturation value.
     */
    public int retrieveSaturation(final int red, final int green, final int blue) {
        return getMaximum(red, green, blue) - getMinimum(red, green, blue);
    }

    /**
     * Calculate normalized value of the hue - saturation for the blue color.
     *
     * @param hue        Hue value.
     * @param saturation Saturation value.
     *
     * @return normalized value
     */
    public int getNHSBlue(final int hue, final int saturation) {
        return ((hue < DefineConstants.B_HUE_MAX && hue > DefineConstants.B_HUE_MIN)
                && saturation > DefineConstants.B_SAT_MIN) ? 255 : 0;
    }

    /**
     * Calculate normalized value of the hue - saturation for the red color.
     *
     * @param hue        Hue value.
     * @param saturation Saturation value.
     *
     * @return Normalised value of the color.
     */
    public int getNHSRed(final int hue, final int saturation) {
        return ((hue < DefineConstants.R_HUE_MAX
                || hue > DefineConstants.R_HUE_MIN)
                && saturation > DefineConstants.R_SAT_MIN) ? 255 : 0;
    }

    /**
     * It calculates theta bases on the equation provided in Valentine thesis.
     *
     * @param red   Red color component.
     * @param green Green color component.
     * @param blue  Blue color component.
     *
     * @return The returned theta is radian.
     */
    private double retrieveTheta(final int red, final int green, final int blue) {

        // The numerator part of equation
        final double numerator = red - (green * 0.5) - (blue * 0.5);

        // The denominator part of equation
        final double denominator
                = (red * red) + (green * green) + (blue * blue) - (red * green) - (red * blue) - (green * blue);

        // This is heavy operation, takes up to 300ms!
        final double value
                = numerator / TabularDictionary.getSqrtOfValue((int) denominator) /*Math.sqrt(denominator)*/;

        int valInt = (int) (value * 1000);

        if (valInt > 1000) {
            valInt = 1000;
        }
        if (valInt < -1000) {
            valInt = -1000;
        }

        return TabularDictionary.getACosOfValue(2000 - 1000 + valInt);
        //return Math.acos(value);
    }

    /**
     * Calculates maximum value over provided values.
     *
     * @param red   Red color component.
     * @param green Green color component.
     * @param blue  Blue color component.
     *
     * @return The greatest value of three passed arguments.
     */
    private int getMaximum(final int red, final int green, final int blue) {
        if (red >= green) {
            if (red >= blue) {
                return red;
            } else {
                return blue;
            }
        } else {
            if (green >= blue) {
                return green;
            } else {
                return blue;
            }
        }
    }

    /**
     * Calculates minimum value over provided values.
     *
     * @param red   Red color component.
     * @param green Green color component.
     * @param blue  Blue color component.
     *
     * @return The lowest value of three passed arguments.
     */
    private int getMinimum(int red, int green, int blue) {
        if (red <= green) {
            if (red <= blue) {
                return red;
            } else {
                return blue;
            }
        } else {
            if (green <= blue) {
                return green;
            } else {
                return blue;
            }
        }
    }
}
