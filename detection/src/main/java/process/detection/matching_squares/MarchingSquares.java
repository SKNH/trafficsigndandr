/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.detection.matching_squares;

import java.util.ArrayList;
import java.util.List;

import static process.detection.matching_squares.Direction.*;

/**
 * A simple implementation of the marching squares algorithm that can identify
 * perimeters in an supplied byte array. The array of data over which this
 * instances of this class operate is not cloned by this class's constructor
 * (for obvious efficiency reasons) and should therefore not be modified while
 * the object is in use. It is expected that the data elements supplied to the
 * algorithm have already been thresholded. The algorithm only distinguishes
 * between zero and non-zero values.
 *
 * @author Tom Gibara
 */

public class MarchingSquares {

    private final int[] data;
    private final int width;
    private final int height;
    private final int[][] visited;

    // constructors

    /**
     * Creates a new object that can locate perimeter paths in the supplied
     * data. The length of the supplied data array must exceed width * height,
     * with the data elements in row major order and the top-left-hand data
     * element at index zero.
     *
     * @param width  the width of the data matrix
     * @param height the height of the data matrix
     * @param data   the data elements
     */

    public MarchingSquares(int width, int height, int[] data) {
        this.width = width;
        this.height = height;
        this.data = data;

        //AppLogger.printMessage("W:" + width + " H:" + height);
        visited = new int[width + 1][height + 1];
    }

    // accessors

    /**
     * @return the width of the data matrix over which this object is operating
     */

    public int getWidth() {
        return width;
    }

    /**
     * @return the width of the data matrix over which this object is operating
     */

    public int getHeight() {
        return height;
    }

    /**
     * @return the data matrix over which this object is operating
     */

    public int[] getData() {
        return data;
    }

    // methods

    /**
     * Finds the perimeter between a set of zero and non-zero values which
     * begins at the specified data element. If no initial point is known,
     * consider using the convenience method supplied. The paths returned by
     * this method are always closed.
     *
     * @param initialX the column of the data matrix at which to start tracing the
     *                 perimeter
     * @param initialY the row of the data matrix at which to start tracing the
     *                 perimeter
     * @return a closed, anti-clockwise path that is a perimeter of between a
     * set of zero and non-zero values in the data.
     * @throws IllegalArgumentException if there is no perimeter at the specified initial point.
     */

    // TODO could be made more efficient by accumulating value during movement
    public Path identifyPerimeter(int initialX, int initialY) {

        if (visited[initialX][initialY] == 1) {
            return null;
        }

        if (initialX < 0) initialX = 0;
        if (initialX > width) initialX = width;
        if (initialY < 0) initialY = 0;
        if (initialY > height) initialY = height;

        final int initialValue = value(initialX, initialY);
        if (initialValue == 0 || initialValue == 15)
            //throw new IllegalArgumentException(String.format("Supplied initial coordinates (%d, %d) do not lie " +
            //       "on a perimeter.", initialX, initialY));
            return null;

        final ArrayList<Direction> directions = new ArrayList<Direction>();

        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;
        final int[] topPoint = new int[2];
        final int[] bottomPoint = new int[2];
        final int[] leftPoint = new int[2];
        final int[] rightPoint = new int[2];

        int x = initialX;
        int y = initialY;
        Direction previous = null;

        do {
            final Direction direction;
            switch (value(x, y)) {
                case 1:
                    direction = N;
                    break;
                case 2:
                    direction = E;
                    break;
                case 3:
                    direction = E;
                    break;
                case 4:
                    direction = W;
                    break;
                case 5:
                    direction = N;
                    break;
                case 6:
                    direction = previous == N ? W : E;
                    break;
                case 7:
                    direction = E;
                    break;
                case 8:
                    direction = S;
                    break;
                case 9:
                    direction = previous == E ? N : S;
                    break;
                case 10:
                    direction = S;
                    break;
                case 11:
                    direction = S;
                    break;
                case 12:
                    direction = W;
                    break;
                case 13:
                    direction = N;
                    break;
                case 14:
                    direction = W;
                    break;
                default:
                    throw new IllegalStateException();
            }
            directions.add(direction);

            visited[x][y] = 1;

            x += direction.screenX;
            y += direction.screenY; // accommodate change of basis

            if (x < minX) {
                minX = x;
                leftPoint[0] = x;
                leftPoint[1] = y;
            }

            if (x > maxX) {
                maxX = x;
                rightPoint[0] = x;
                rightPoint[1] = y;
            }

            if (y < minY) {
                minY = y;
                topPoint[0] = x;
                topPoint[1] = y;
            }

            if (y > maxY) {
                maxY = y;
                bottomPoint[0] = x;
                bottomPoint[1] = y;
            }

            previous = direction;
        } while (x != initialX || y != initialY);

        final Path path = new Path(initialX, -initialY, directions);
        path.setMaxX(maxX);
        path.setMinX(minX);
        path.setMaxY(maxY);
        path.setMinY(minY);
        path.setShapeWidth(maxX - minX);
        path.setTopPoint(topPoint);
        path.setBottomPoint(bottomPoint);
        path.setLeftPoint(leftPoint);
        path.setRightPoint(rightPoint);
        return path;
    }

    /**
     * A convenience method that locates at least one perimeter in the data with
     * which this object was constructed. If there is no perimeter (ie. if all
     * elements of the supplied array are identically zero) then null is
     * returned.
     *
     * @return a perimeter path obtained from the data, or null
     */
    public List<Path> identifyPerimeter() {
        final int size = width * height;
        final List<Path> arrayList = new ArrayList<>();
        Path path;
        for (int i = 0; i < size; i++) {
            if (data[i] != 0) {
                path = identifyPerimeter(i % width, i / width);
                if (path == null) {
                    continue;
                }
                arrayList.add(path);
            }
        }
        return arrayList;
    }

    // private utility methods

    private int value(final int xCoordinate, final int yCoordinate) {
        int sum = 0;
        if (isSet(xCoordinate, yCoordinate)) sum |= 1;
        if (isSet(xCoordinate + 1, yCoordinate)) sum |= 2;
        if (isSet(xCoordinate, yCoordinate + 1)) sum |= 4;
        if (isSet(xCoordinate + 1, yCoordinate + 1)) sum |= 8;
        return sum;
    }

    private boolean isSet(final int xCoordinate, final int yCoordinate) {
        return !(xCoordinate <= 0 || xCoordinate > width || yCoordinate <= 0 || yCoordinate > height)
                && data[(yCoordinate - 1) * width + (xCoordinate - 1)] != 0;
    }
}
