/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.detection.matching_squares;

/**
 * A direction in the plane. As a convenience, directions provide unit vector
 * components (manhattan metric) for both the conventional plane and screen
 * coordinates (y axis reversed).
 *
 * @author Tom Gibara
 */

public enum Direction {

    E(1, 0), NE(1, 1),

    N(0, 1), NW(-1, 1),

    W(-1, 0), SW(-1, -1),

    S(0, -1), SE(1, -1);

    /**
     * Constant value of the Math.sqrt(2.0) / 2.0
     */
    private static final double PI_DIVIDED_BY_TWO = 0.7071067811865476;

    /**
     * The horizontal distance moved in this direction within the plane.
     */
    public final int planeX;

    /**
     * The vertical distance moved in this direction within the plane.
     */
    public final int planeY;

    /**
     * The horizontal distance moved in this direction in screen coordinates.
     */
    public final int screenX;

    /**
     * The vertical distance moved in this direction in screen coordinates.
     */
    public final int screenY;

    /**
     * The euclidean length of this direction's vectors.
     */
    public final double length;

    /**
     * Private constructor.
     *
     * @param xCoordinate X coordinate.
     * @param yCoordinate Y coordinate.
     */
    private Direction(final int xCoordinate, final int yCoordinate) {
        planeX = xCoordinate;
        planeY = yCoordinate;
        screenX = xCoordinate;
        screenY = -yCoordinate;
        length = xCoordinate != 0 && yCoordinate != 0 ? PI_DIVIDED_BY_TWO : 1.0;
    }
}
