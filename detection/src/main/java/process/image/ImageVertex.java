/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package process.image;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/14/13
 * Time: 7:44 PM
 */

/**
 * This is a class that represent all properties of a single pixel
 */
public class ImageVertex {

    private int leader = -1;
    private int redColor;
    private int greenColor;
    private int blueColor;
    private int xPosition;
    private int yPosition;
    private int hue;
    private int saturation;
    private int luminance;
    /**
     * Normalized Hues Saturation of the Blue color
     */
    private int nhsBlue;
    /**
     * Normalized Hues Saturation of the Red color
     */
    private int nhsRed;

    /**
     * Default constructor.
     */
    public ImageVertex() {

    }

    public void setLeader(int value) {
        leader = value;
    }

    public int getLeader() {
        return leader;
    }

    public void setRedColor(int value) {
        redColor = value;
    }

    public void setGreenColor(int value) {
        greenColor = value;
    }

    public void setBlueColor(int value) {
        blueColor = value;
    }

    public void setxPosition(int value) {
        xPosition = value;
    }

    public void setyPosition(int value) {
        yPosition = value;
    }

    public void setHue(int value) {
        hue = value;
    }

    public void setSaturation(int value) {
        saturation = value;
    }

    public void setLuminance(int value) {
        luminance = value;
    }

    public void setNhsBlue(int value) {
        nhsBlue = value;
    }

    /**
     * @return Red color
     */
    public int getRedColor() {
        return redColor;
    }

    /**
     * @return Green color
     */
    public int getGreenColor() {
        return greenColor;
    }

    /**
     * @return Blue color
     */
    public int getBlueColor() {
        return blueColor;
    }

    /**
     * @return X position
     */
    public int getXPosition() {
        return xPosition;
    }

    /**
     * @return Y position
     */
    public int getYPosition() {
        return yPosition;
    }

    /**
     * @return Hue value
     */
    public int getHue() {
        return hue;
    }

    /**
     * @return Saturation value
     */
    public int getSaturation() {
        return saturation;
    }

    /**
     * @return Luminance value
     */
    public int getLuminance() {
        return luminance;
    }

    /**
     * @return Normalized Hue Saturation value of the Blue color
     */
    public int getNhsBlue() {
        return nhsBlue;
    }

    /**
     * @return Normalized Hue Saturation value of the Red color
     */
    public int getNhsRed() {
        return nhsRed;
    }

    /**
     * Normalized Hue Saturation value of the Red color
     *
     * @param nhsRed Normalized Hue Saturation Red value
     */
    public void setNhsRed(int nhsRed) {
        this.nhsRed = nhsRed;
    }

    /**
     * Reset all values to default.
     */
    public void reset() {
        xPosition = 0;
        yPosition = 0;
        redColor = 0;
        greenColor = 0;
        blueColor = 0;
        hue = 0;
        saturation = 0;
        luminance = 0;
        nhsBlue = 0;
        nhsRed = 0;
    }

    /**
     * Factory method to create instance of the {@link process.image.ImageVertex}.
     *
     * @return Instance of the {@link process.image.ImageVertex}.
     */
    public static ImageVertex createDefaultInstance() {
        return new ImageVertex();
    }
}