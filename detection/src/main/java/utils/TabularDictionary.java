/*
 * Copyright 2015 The "Shape Detection" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */
public class TabularDictionary {

    private static final int SQRT_DIC_SIZE = 70000;
    private static final int ACOS_DIC_HALF_SIZE = 999;

    private static final double[] SQRT_DICTIONARY = new double[SQRT_DIC_SIZE + 1];
    private static final double[] ACOS_DICTIONARY = new double[ACOS_DIC_HALF_SIZE * 2 + 3];

    public static double getSqrtOfValue(int value) {
        return SQRT_DICTIONARY[value];
    }

    public static double getACosOfValue(int value) {
        return ACOS_DICTIONARY[value];
    }

    public static void init() {
        fillACos();
        fillSqrtFromRange();
    }

    private static void fillACos() {
        int n = ACOS_DIC_HALF_SIZE * 2;
        double j = -1;
        BigDecimal bigDecimal;
        ACOS_DICTIONARY[0] = Math.acos(-1);
        for (int i = 0; i <= n; i++) {
            j += 0.001;

            bigDecimal = new BigDecimal(j);
            bigDecimal = bigDecimal.setScale(3, RoundingMode.HALF_DOWN);
            j = bigDecimal.doubleValue();

            ACOS_DICTIONARY[i + 1] = Math.acos(j);
        }
        ACOS_DICTIONARY[n + 1] = Math.acos(1);
    }

    private static void fillSqrtFromRange() {
        for (int i = 0; i <= SQRT_DIC_SIZE; i++) {
            SQRT_DICTIONARY[i] = Math.sqrt(i);
        }
    }
}
