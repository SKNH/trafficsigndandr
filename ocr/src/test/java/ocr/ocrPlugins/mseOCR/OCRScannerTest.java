package ocr.ocrPlugins.mseOCR;

import ocr.ocrPlugins.scanner.PixelImageTest;
import ocr.scanner.accuracy.AccuracyListener;
import ocr.scanner.accuracy.OCRIdentification;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 12:10
 */
public class OCRScannerTest {

    @Test
    public void initialization() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);
    }

    @Test
    public void listenerCallEndRaw() {
        final AccuracyListener listener = mock(AccuracyListener.class);
        final OCRScanner ocrScanner = new OCRScanner();
        ocrScanner.acceptAccuracyListener(listener);

        ocrScanner.endRow(PixelImageTest.getPixelImage(), PixelImageTest.WIDTH, PixelImageTest.HEIGHT);

        verify(listener, times(1)).processCharOrSpace(any(OCRIdentification.class));
    }

    @Test
    public void listenerCallProcessSpace() {
        final AccuracyListener listener = mock(AccuracyListener.class);
        final OCRScanner ocrScanner = new OCRScanner();
        ocrScanner.acceptAccuracyListener(listener);

        ocrScanner.processSpace(PixelImageTest.getPixelImage(), 0, 0, 0, 0);

        verify(listener, times(1)).processCharOrSpace(any(OCRIdentification.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void processCharWithNullPixelImageThrowException() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);

        ocrScanner.processChar(null, 0, 0, 0, 0, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void scanWithNullPixelImageThrowException() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);

        ocrScanner.scan(null, 0, 0, 0, 0, 0, 0, new CharacterRange[10]);
    }

    @Test
    public void scanWithNullCharactersRangeThrowException() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);

        ocrScanner.scan(new int[100], 10, 10, 0, 0, 0, 0, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void scanWithNullPixelImageAndCharactersRangeThrowException() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);

        ocrScanner.scan(null, 0, 0, 0, 0, 0, 0, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addTrainingImagesWithNullMapThrowException() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);

        ocrScanner.addTrainingImages(null);
    }

    @Test
    public void addTrainingImagesWithMap() {
        final OCRScanner ocrScanner = new OCRScanner();

        assertNotNull(ocrScanner);

        ocrScanner.addTrainingImages(new HashMap<Character, ArrayList<TrainingImage>>());
    }
}