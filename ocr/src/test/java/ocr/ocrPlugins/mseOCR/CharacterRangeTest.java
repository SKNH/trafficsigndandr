package ocr.ocrPlugins.mseOCR;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 8:58
 */
public class CharacterRangeTest {

    @Test
    public void initialization() {
        final int min = 0;
        final int max = 100;
        final CharacterRange characterRange = new CharacterRange(min, max);

        assertNotNull(characterRange);
    }

    @Test(expected = IllegalArgumentException.class)
    public void wrongMinAndMaxValuesThrowException() {
        final int min = 0;
        final int max = 100;
        new CharacterRange(max, min);
    }

    @Test
    public void sameMinAndMaxValues() {
        final int max = 100;
        final CharacterRange characterRange = new CharacterRange(max, max);

        assertNotNull(characterRange);
    }

    @Test
    public void getMinValue() {
        final int min = 0;
        final int max = 100;
        final CharacterRange characterRange = new CharacterRange(min, max);

        assertNotNull(characterRange);
        assertEquals(min, characterRange.getMinValue());
    }

    @Test
    public void getMaxValue() {
        final int min = 0;
        final int max = 100;
        final CharacterRange characterRange = new CharacterRange(min, max);

        assertNotNull(characterRange);
        assertEquals(max, characterRange.getMaxValue());
    }
}