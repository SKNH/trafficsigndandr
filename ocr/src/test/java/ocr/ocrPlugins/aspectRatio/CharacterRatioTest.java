package ocr.ocrPlugins.aspectRatio;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 18.10.14
 * Time: 23:42
 */
public class CharacterRatioTest {

    private static final char CHARACTER_A = "a".charAt(0);
    private static final char CHARACTER_B = "b".charAt(0);
    private static final int CHARACTER_A_RATIO = 80;
    private static final int CHARACTER_B_RATIO = 60;

    @Test
    public void initialize() {
        final CharacterRatio characterRatio = new CharacterRatio(CHARACTER_A, CHARACTER_A_RATIO);

        assertNotNull(characterRatio);
    }

    @Test
    public void characterAndRationAreCorrect() {
        final CharacterRatio characterRatio = new CharacterRatio(CHARACTER_A, CHARACTER_A_RATIO);

        assertNotNull(characterRatio);
        assertEquals(CHARACTER_A, characterRatio.getCharacter());
        assertEquals(CHARACTER_A_RATIO, characterRatio.getRatio(), 0);
    }

    @Test
    public void compareWithOtherCharacter() {
        final CharacterRatio characterRatio_A = new CharacterRatio(CHARACTER_A, CHARACTER_A_RATIO);
        final CharacterRatio characterRatio_B = new CharacterRatio(CHARACTER_B, CHARACTER_B_RATIO);

        assertNotNull(characterRatio_A);
        assertNotNull(characterRatio_B);
        assertEquals(-1, characterRatio_B.compareTo(characterRatio_A));
        assertEquals(1, characterRatio_A.compareTo(characterRatio_B));
        assertEquals(0, characterRatio_B.compareTo(characterRatio_B));
    }
}