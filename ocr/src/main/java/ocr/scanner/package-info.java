/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 18:16
 */

/**
 * This package contains classes and interfaces responsible for the scanning process.
 */
package ocr.scanner;
