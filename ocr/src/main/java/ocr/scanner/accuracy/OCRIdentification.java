/**
 * OCRIdentification.java
 * Copyright (c) 2003-2010 Ronald B. Cemer
 * Modified by William Whitney
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.scanner.accuracy;

import java.util.ArrayList;

/**
 * Provides a data structure to manage an OCR character recognition attempt.
 *
 * @author William Whitney
 */
public class OCRIdentification {

    /**
     * List of the characters.
     */
    private final ArrayList<Character> chars = new ArrayList<>();

    /**
     * List of errors.
     */
    private final ArrayList<Double> identErrors = new ArrayList<>();

    /**
     * {@link ocr.scanner.accuracy.OCRComp}.
     */
    private OCRComp ocrTypeValue;

    /**
     * Constructor.
     *
     * @param ocrType {@link ocr.scanner.accuracy.OCRComp}
     */
    public OCRIdentification(final OCRComp ocrType) {
        ocrTypeValue = ocrType;
    }

    /**
     * This method allows to add single character.
     *
     * @param newChar    Character.
     * @param identError Error.
     */
    public final void addChar(final char newChar, final double identError) {
        chars.add(newChar);
        identErrors.add(identError);
    }

    @Override
    public final String toString() {
        String retStr = "----- OCRIdentification  -----\n";
        retStr += "OCR Type: " + ocrTypeValue + "\n";
        for (int i = 0; i < chars.size(); i++) {
            retStr += "Char: " + chars.get(i) + " " + identErrors.get(i) + "\n";
        }
        return retStr;
    }

    /**
     * Factory method to create space identification for a given {@link ocr.scanner.accuracy.OCRComp}.
     *
     * @param ocrComp {@link ocr.scanner.accuracy.OCRComp}
     * @return Instance of the {@link ocr.scanner.accuracy.OCRIdentification}
     */
    public static OCRIdentification getSpaceIdentification(final OCRComp ocrComp) {
        final OCRIdentification identification = getIdentification(ocrComp);
        identification.addChar(' ', 0.0);
        return identification;
    }

    /**
     * Factory method to create new line identification for a given {@link ocr.scanner.accuracy.OCRComp}.
     *
     * @param ocrComp {@link ocr.scanner.accuracy.OCRComp}
     * @return Instance of the {@link ocr.scanner.accuracy.OCRIdentification}
     */
    public static OCRIdentification getNewLineIdentification(final OCRComp ocrComp) {
        final OCRIdentification identification = getIdentification(ocrComp);
        identification.addChar('\n', 0.0);
        return identification;
    }

    /**
     * Factory method to create empty identification for a given {@link ocr.scanner.accuracy.OCRComp}.
     *
     * @param ocrComp {@link ocr.scanner.accuracy.OCRComp}
     * @return Instance of the {@link ocr.scanner.accuracy.OCRIdentification}
     */
    public static OCRIdentification getIdentification(final OCRComp ocrComp) {
        return new OCRIdentification(ocrComp);
    }
}
