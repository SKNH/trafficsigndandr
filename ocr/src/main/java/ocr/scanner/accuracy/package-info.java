/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 19.10.14
 * Time: 18:02
 */

/**
 * This package contains interfaces responsible for the accuracy.
 */
package ocr.scanner.accuracy;
