/**
 * OCRScanner.java
 * Copyright (c) 2003-2010 Ronald B. Cemer
 * Modified by William Whitney
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.ocrPlugins.mseOCR;

import ocr.scanner.DocumentScanner;
import ocr.scanner.DocumentScannerListenerAdaptor;
import ocr.scanner.PixelImage;
import ocr.scanner.accuracy.AccuracyListener;
import ocr.scanner.accuracy.AccuracyProvider;
import ocr.scanner.accuracy.OCRComp;
import ocr.scanner.accuracy.OCRIdentification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * OCR document scanner.
 *
 * @author Ronald B. Cemer
 */
public class OCRScanner extends DocumentScannerListenerAdaptor implements AccuracyProvider {

    /**
     * Constant defines count of the best matches.
     */
    private static final int BEST_MATCH_STORE_COUNT = 8;

    /**
     * New line separator value.
     */
    private static final String NEWLINE = System.getProperty("line.separator");

    /**
     * Decoder buffer.
     */
    private final StringBuffer decodeBuffer = new StringBuffer();

    /**
     * Map of the training images.
     */
    private final HashMap<Character, ArrayList<TrainingImage>> trainingImages = new HashMap<>();

    /**
     * Array of the best characters.
     */
    private final Character[] bestChars = new Character[BEST_MATCH_STORE_COUNT];

    /**
     * Array of the best MSEs.
     */
    private final double[] bestMSEs = new double[BEST_MATCH_STORE_COUNT];

    /**
     * Instance of the {@link ocr.scanner.DocumentScanner}.
     */
    private final DocumentScanner documentScanner = new DocumentScanner();

    /**
     * Array of the acceptable characters values.
     */
    private CharacterRange[] acceptableCharsValue;

    /**
     * Boolean that indicates whether the raw is first.
     */
    private boolean firstRow = false;

    /**
     * Implementation of the {@link ocr.scanner.accuracy.AccuracyListener}.
     */
    private AccuracyListener accuracyListener;

    /**
     * Set an implementation of the {@link ocr.scanner.accuracy.AccuracyListener}.
     *
     * @param value implementation of the {@link ocr.scanner.accuracy.AccuracyListener}
     */
    public final void acceptAccuracyListener(final AccuracyListener value) {
        accuracyListener = value;
    }

    /**
     * @return The <code>DocumentScanner</code> instance that is used to scan the document(s).
     * This is useful if the caller wants to adjust some of the scanner's parameters.
     */
    public final DocumentScanner getDocumentScanner() {
        return documentScanner;
    }

    /**
     * Remove all training images from the training set.
     */
    public final void clearTrainingImages() {
        trainingImages.clear();
    }

    /**
     * Add training images to the training set.
     *
     * @param images A <code>HashMap</code> using <code>Character</code>s for
     *               the keys.  Each value is an <code>ArrayList</code> of
     *               <code>TrainingImages</code> for the specified character.  The training
     *               images are added to any that may already have been loaded.
     */
    public final void addTrainingImages(final HashMap<Character, ArrayList<TrainingImage>> images) {
        if (images == null) {
            throw new IllegalArgumentException(getClass().getSimpleName() + " addTrainingImages -> images are null");
        }

        ArrayList<TrainingImage> al;
        ArrayList<TrainingImage> oldAl;
        for (Character key : images.keySet()) {
            al = images.get(key);
            oldAl = trainingImages.get(key);
            if (oldAl == null) {
                oldAl = new ArrayList<>();
                trainingImages.put(key, oldAl);
            }
            for (TrainingImage anAl : al) {
                oldAl.add(anAl);
            }
        }
    }

    /**
     * Scan an image and return the decoded text.
     *
     * @param pixels          An array of pixels.  This can be in RGBA or grayscale.
     *                        By default, it is RGBA, but if the <code>toGrayScale()</code> method
     *                        has been called, each pixel will be in the range of 0-255 grayscale.
     * @param width           Width of the image, in pixels.
     * @param height          Height of the image, in pixels.
     * @param x1              The leftmost pixel position of the area to be scanned, or
     *                        <code>0</code> to start scanning at the left boundary of the image.
     * @param y1              The topmost pixel position of the area to be scanned, or
     *                        <code>0</code> to start scanning at the top boundary of the image.
     * @param x2              The rightmost pixel position of the area to be scanned, or
     *                        <code>0</code> to stop scanning at the right boundary of the image.
     * @param y2              The bottommost pixel position of the area to be scanned, or
     *                        <code>0</code> to stop scanning at the bottom boundary of the image.
     * @param acceptableChars An array of <code>CharacterRange</code> objects
     *                        representing the ranges of characters which are allowed to be decoded,
     *                        or <code>null</code> to not limit which characters can be decoded.
     * @return The decoded text.
     */
    public final String scan(
            final int[] pixels,
            final int width,
            final int height,
            final int x1,
            final int y1,
            final int x2,
            final int y2,
            final CharacterRange[] acceptableChars) {

        if (pixels == null) {
            throw new IllegalArgumentException(getClass().getSimpleName() + " scan -> pixels are null");
        }

        acceptableCharsValue = acceptableChars;
        final PixelImage pixelImage = new PixelImage(pixels, width, height);
        pixelImage.toGrayScale(true);
        pixelImage.filter();
        decodeBuffer.setLength(0);
        firstRow = true;
        documentScanner.scan(pixelImage, this, x1, y1, x2, y2);
        final String result = decodeBuffer.toString();
        decodeBuffer.setLength(0);
        return result;
    }

    @Override
    public final void endRow(final PixelImage pixelImage, final int y1, final int y2) {
        //Send accuracy of this identification to the listener
        if (accuracyListener == null) {
            return;
        }
        accuracyListener.processCharOrSpace(OCRIdentification.getNewLineIdentification(OCRComp.MSE));
    }

    @Override
    public final void beginRow(final PixelImage pixelImage, final int y1, final int y2) {
        if (firstRow) {
            firstRow = false;
        } else {
            decodeBuffer.append(NEWLINE);
        }
    }

    @Override
    public final void processChar(
            final PixelImage pixelImage,
            final int x1,
            final int y1,
            final int x2,
            final int y2,
            final int rowY1,
            final int rowY2) {
        if (pixelImage == null) {
            throw new IllegalArgumentException(getClass().getSimpleName() + " process char -> PixelImage is null");
        }

        final int[] pixels = pixelImage.getPixels();
        final int width = pixelImage.getWidth();
        final int height = pixelImage.getHeight();
        final int areaW = x2 - x1, areaH = y2 - y1;
        final float aspectRatio = ((float) areaW) / ((float) areaH);
        final int rowHeight = rowY2 - rowY1;
        final float topWhiteSpaceFraction = (float) (y1 - rowY1) / (float) rowHeight;
        final float bottomWhiteSpaceFraction = (float) (rowY2 - y2) / (float) rowHeight;
        Iterator<Character> characterIterator;
        if (acceptableCharsValue != null) {
            ArrayList<Character> al = new ArrayList<Character>();
            for (CharacterRange cr : acceptableCharsValue) {
                for (int c = cr.getMinValue(); c <= cr.getMaxValue(); c++) {
                    Character ch = new Character((char) c);
                    if (al.indexOf(ch) < 0) {
                        al.add(ch);
                    }
                }
            }
            characterIterator = al.iterator();
        } else {
            characterIterator = trainingImages.keySet().iterator();
        }
        int bestCount = 0;
        while (characterIterator.hasNext()) {
            Character ch = characterIterator.next();
            ArrayList<TrainingImage> al = trainingImages.get(ch);
            if (al.isEmpty()) {
                continue;
            }
            double mse = 0.0;
            boolean gotAny = false;
            for (TrainingImage ti : al) {
                if (!isTrainingImageACandidate(
                        aspectRatio,
                        areaW,
                        areaH,
                        topWhiteSpaceFraction,
                        bottomWhiteSpaceFraction,
                        ti)) {
                    continue;
                }
                double thisMSE = ti.calcMSE(pixels, width, height, x1, y1, x2, y2);
                if (!gotAny || thisMSE < mse) {
                    gotAny = true;
                    mse = thisMSE;
                }
            }
            /// Maybe mse should be required to be below a certain threshold before we store characterIterator.
            /// That would help us to handle things like welded characters, and characters that get improperly
            /// split into two or more characters.
            if (!gotAny) {
                continue;
            }
            boolean inserted = false;
            for (int i = 0; i < bestCount; i++) {
                if (mse < bestMSEs[i]) {
                    for (int j = Math.min(bestCount, BEST_MATCH_STORE_COUNT - 1); j > i; j--) {
                        int k = j - 1;
                        bestChars[j] = bestChars[k];
                        bestMSEs[j] = bestMSEs[k];
                    }
                    bestChars[i] = ch;
                    bestMSEs[i] = mse;
                    if (bestCount < BEST_MATCH_STORE_COUNT) {
                        bestCount++;
                    }
                    inserted = true;
                    break;
                }
            }
            if (!inserted && bestCount < BEST_MATCH_STORE_COUNT) {
                bestChars[bestCount] = ch;
                bestMSEs[bestCount] = mse;
                bestCount++;
            }
        }
        /// We could also put some aspect ratio range checking into the page scanning logic (but only when
        /// decoding; not when loading training images) so that the aspect ratio of a non-empty character
        /// block is limited to within the min and max of the aspect ratios in the training set.
        if (bestCount > 0) {
            decodeBuffer.append(bestChars[0].charValue());

            //Send accuracy of this identification to the listener
            if (accuracyListener == null) {
                return;
            }
            final OCRIdentification identAccuracy = OCRIdentification.getIdentification(OCRComp.MSE);
            for (int i = 0; i < bestCount; i++) {
                identAccuracy.addChar(bestChars[i], bestMSEs[i]);
            }
            accuracyListener.processCharOrSpace(identAccuracy);

            return;
        }

        if (accuracyListener == null) {
            return;
        }
        accuracyListener.processCharOrSpace(OCRIdentification.getIdentification(OCRComp.MSE));
    }

    /**
     * This method check whether training image is a candidate.
     *
     * @param aspectRatio              Aspect ratio
     * @param w                        Width
     * @param h                        Height
     * @param topWhiteSpaceFraction    Top white space fraction
     * @param bottomWhiteSpaceFraction Bottom white space fraction
     * @param trainingImage            Instance of the {@link ocr.ocrPlugins.mseOCR.TrainingImage}
     * @return true or false
     */
    private boolean isTrainingImageACandidate(
            final float aspectRatio,
            final int w,
            final int h,
            final float topWhiteSpaceFraction,
            final float bottomWhiteSpaceFraction,
            final TrainingImage trainingImage) {
        if (trainingImage == null) {
            return false;
        }

        // The aspect ratios must be within tolerance.
        if (((aspectRatio / trainingImage.getAspectRatio()) - 1.0f) > TrainingImage.ASPECT_RATIO_TOLERANCE) {
            return false;
        }
        if (((trainingImage.getAspectRatio() / aspectRatio) - 1.0f) > TrainingImage.ASPECT_RATIO_TOLERANCE) {
            return false;
        }
        // The top whitespace fractions must be within tolerance.
        if (Math.abs(topWhiteSpaceFraction - trainingImage.getTopWhiteSpaceFraction())
                > TrainingImage.TOP_WHITE_SPACE_FRACTION_TOLERANCE) {
            return false;
        }
        // The bottom whitespace fractions must be within tolerance.
        if (Math.abs(bottomWhiteSpaceFraction - trainingImage.getBottomWhiteSpaceFraction())
                > TrainingImage.BOTTOM_WHITE_SPACE_FRACTION_TOLERANCE) {
            return false;
        }
        // If the area being scanned is really small and we
        // are about to crunch down a training image by a huge
        // factor in order to compare to it, then don't do that.

        final int minFactor = 4;
        final int maxFactor = 10;
        if (w <= minFactor && (trainingImage.getWidth() >= (w * maxFactor))) {
            return false;
        }
        if (h <= minFactor && (trainingImage.getHeight() >= (h * maxFactor))) {
            return false;
        }
        // If the area being scanned is really large and we
        // are about to expand a training image by a huge
        // factor in order to compare to it, then don't do that.
        return !(trainingImage.getWidth() <= minFactor && (w >= (trainingImage.getWidth() * maxFactor)))
                && !(trainingImage.getHeight() <= minFactor && (h >= (trainingImage.getHeight() * maxFactor)));
    }

    @Override
    public final void processSpace(final PixelImage pixelImage,
                                   final int x1, final int y1, final int x2, final int y2) {
        decodeBuffer.append(' ');
        //Send accuracy of this identification to the listener
        if (accuracyListener == null) {
            return;
        }
        accuracyListener.processCharOrSpace(OCRIdentification.getSpaceIdentification(OCRComp.MSE));
    }
}
