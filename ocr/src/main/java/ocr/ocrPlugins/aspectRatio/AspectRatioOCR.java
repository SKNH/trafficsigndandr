/**
 * AspectRatioOCR.java
 * Copyright (c) 2010 William Whitney
 * All rights reserved.
 * This software is released under the BSD license.
 * Please see the accompanying LICENSE.txt for details.
 */
package ocr.ocrPlugins.aspectRatio;

import ocr.ocrPlugins.mseOCR.TrainingImage;
import ocr.scanner.DocumentScannerListenerAdaptor;
import ocr.scanner.PixelImage;
import ocr.scanner.accuracy.AccuracyListener;
import ocr.scanner.accuracy.AccuracyProvider;
import ocr.scanner.accuracy.OCRComp;
import ocr.scanner.accuracy.OCRIdentification;

import java.util.*;

/**
 * Provides an OCR that can be used in addition to other OCR plug-ins to
 * increase accuracy.
 *
 * @author William Whitney
 */
public class AspectRatioOCR extends DocumentScannerListenerAdaptor implements AccuracyProvider {

    /**
     * Interface for the accuracy listening.
     */
    private AccuracyListener listener;

    /**
     * List of the chars ratios.
     */
    private final List<CharacterRatio> charRatioList = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param trainingImagesMap Map of the training images
     */
    public AspectRatioOCR(final HashMap<Character, ArrayList<TrainingImage>> trainingImagesMap) {
        if (trainingImagesMap == null) {
            throw new IllegalArgumentException(getClass().getSimpleName()
                    + " Constructor -> training images map is null");
        }
        // Processing training images, not sure if the Constructor is a correct place
        processTrainingImages(trainingImagesMap);
    }

    /**
     * Set accuracy listener.
     *
     * @param value Implementation of the {@link ocr.scanner.accuracy.AccuracyListener}
     */
    public final void acceptAccuracyListener(final AccuracyListener value) {
        listener = value;
    }

    @Override
    public final void endRow(final PixelImage pixelImage, final int y1, final int y2) {
        //Send accuracy of this identification to the listener
        if (listener == null) {
            return;
        }
        listener.processCharOrSpace(OCRIdentification.getSpaceIdentification(OCRComp.ASPECT_RATIO));
    }

    @Override
    public final void processChar(final PixelImage pixelImage, final int x1, final int y1,
                            final int x2, final int y2, final int rowY1, final int rowY2) {
        if (listener == null) {
            return;
        }
        final int width = x2 - x1;
        final int height = y2 - y1;
        final double currRatio = getRatio(width, height);
        listener.processCharOrSpace(determineCharacterPossibilities(currRatio));
    }

    @Override
    public final void processSpace(final PixelImage pixelImage,
                                   final int x1, final int y1, final int x2, final int y2) {
        if (listener == null) {
            return;
        }
        listener.processCharOrSpace(OCRIdentification.getSpaceIdentification(OCRComp.ASPECT_RATIO));
    }

    /**
     * This method return {@link ocr.ocrPlugins.aspectRatio.CharacterRatio} from the specified position.
     * @param position Position of the {@link ocr.ocrPlugins.aspectRatio.CharacterRatio}
     * @return {@link ocr.ocrPlugins.aspectRatio.CharacterRatio} from the specified position or null is List is empty
     */
    protected final CharacterRatio getCharacterRatioAt(final int position) {
        if (charRatioList.isEmpty()) {
            return null;
        }
        return charRatioList.get(position);
    }
    /**
     * Process training image.
     *
     * @param trainingImages Map of the training images
     */
    private void processTrainingImages(final HashMap<Character, ArrayList<TrainingImage>> trainingImages) {
        ArrayList<TrainingImage> charTrainingImages;
        for (Character key : trainingImages.keySet()) {
            charTrainingImages = trainingImages.get(key);
            if (charTrainingImages != null) {
                for (TrainingImage charTrainingImage : charTrainingImages) {
                    charRatioList.add(
                            new CharacterRatio(key, getRatio(charTrainingImage.getWidth(),
                                    charTrainingImage.getHeight())));
                }
            }
        }

        Collections.sort(charRatioList);
    }

    /**
     * Get ratio.
     *
     * @param width  Width of the image
     * @param height Height of the image
     * @return Value of the ratio
     */
    private double getRatio(final int width, final int height) {
        return ((double) width) / ((double) height);
    }

    /**
     * Determine character possibilities.
     *
     * @param targetRatio Target ratio.
     * @return possibility
     */
    private OCRIdentification determineCharacterPossibilities(final double targetRatio) {
        double smallestError = Double.MAX_VALUE;
        double absValue;
        final Stack<CharacterRatio> bestResults = new Stack<>();
        for (CharacterRatio currChar : charRatioList) {
            absValue = Math.abs(currChar.getRatio() - targetRatio);
            if (absValue < smallestError) {
                smallestError = absValue;
                bestResults.push(currChar);
            }
        }

        final OCRIdentification newIdent = new OCRIdentification(OCRComp.ASPECT_RATIO);

        int count = 0;
        final int threshold = 5;
        while (!bestResults.isEmpty() && count < threshold) {
            final CharacterRatio currChar = bestResults.pop();
            double errorAmount = Math.abs(targetRatio - currChar.getRatio());
            newIdent.addChar(currChar.getCharacter(), errorAmount);
            count++;
        }

        return newIdent;
    }
}
