import utils.AppLogger;
import utils.FileUtilities;
import utils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/13/14
 * Time: 9:15 PM
 */
public class ComparingDemo {

    BufferedImage image_A;
    BufferedImage image_B;

    public ComparingDemo() {

    }

    public void init() throws IOException {
        image_A = FileUtilities.loadImage("assets/images/digits/digit_0_v1.jpg");
        image_B = FileUtilities.loadImage("assets/images/digits/digit_3_v1.jpg");

        BufferedImage image_A_scaled = ImageUtils.resizeImage(image_A, 2, 2, BufferedImage.TYPE_BYTE_GRAY);
        BufferedImage image_B_scaled = ImageUtils.resizeImage(image_B, 2, 2, BufferedImage.TYPE_BYTE_GRAY);

        // Luminance
        //i[0] = (p[0][0] + p[0][1] + p[1][0] + p[1][1])/4

        int luminance_A = ((image_A_scaled.getRGB(0, 0) & 0xFF ) + (image_A_scaled.getRGB(0, 1) & 0xFF) +
                (image_A_scaled.getRGB(1, 0) & 0xFF) + (image_A_scaled.getRGB(1, 1) & 0xFF )) / 4;

        int luminance_B = ((image_B_scaled.getRGB(0, 0) & 0xFF ) + (image_B_scaled.getRGB(0, 1) & 0xFF) +
                (image_B_scaled.getRGB(1, 0) & 0xFF) + (image_B_scaled.getRGB(1, 1) & 0xFF )) / 4;

        AppLogger.printMessage("Luminance A:" + luminance_A);
        AppLogger.printMessage("Luminance B:" + luminance_B);
    }
}