import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import process.ImageProcessingCallbackImpl;
import process.ImageProcessingImpl;
import process.recognition.RecognizedShape;
import utils.AppLogger;
import utils.TabularDictionary;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/6/14
 * Time: 8:49 PM
 */
public class CircleRecognizeTest {

    @Before
    public void setUp() throws Exception {
        TabularDictionary.init();
    }

    @Test
    public void testRecognizeCircles() throws Exception {

        final int[] counter = {0};
        final int signsNumber = 1;

        String fileName;
        ImageProcessingImpl imageProcessing = new ImageProcessingImpl();

        final CountDownLatch countDownLatch = new CountDownLatch(signsNumber);

        imageProcessing.setCallback(new ImageProcessingCallbackImpl(null) {

            @Override
            public void onRecognize(RecognizedShape recognizedShape) {
                AppLogger.printMessage("Result: " + recognizedShape);
                if (recognizedShape.getShape() == RecognizedShape.SHAPE.ELLIPSE) {
                    counter[0]++;
                }
            }

            @Override
            public void onComplete(BufferedImage resultImage) throws IOException {
                super.onComplete(resultImage);

                countDownLatch.countDown();
            }
        });

        for (int i = 1; i < signsNumber + 1; i++) {

            if (i < 10) {
                fileName = "0" + i;
            } else {
                fileName = String.valueOf(i);
            }

            AppLogger.printMessage("");

            InputStream inputStream = this.getClass().getResourceAsStream("circle_" + fileName + ".jpg");

            AppLogger.printMessage("Read image input stream:" + inputStream);

            imageProcessing.read(inputStream);
        }

        countDownLatch.await();

        AppLogger.printMessage("");
        AppLogger.printMessage("Recognize " + counter[0] + " signs from " + signsNumber);

        Assert.assertEquals(signsNumber, counter[0]);
    }
}