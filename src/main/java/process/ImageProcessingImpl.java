package process;

import process.detection.ihls_nhs.RGBtoNHS;
import process.detection.matching_squares.Direction;
import process.detection.matching_squares.MarchingSquares;
import process.detection.matching_squares.Path;
import process.image.ImageDataStructure;
import process.image.ImageVertex;
import process.recognition.CompositeShapeRecognizer;
import process.recognition.ImageRecognizingCallback;
import process.recognition.RecognizedShape;
import process.recognition.shape.EllipseRecognition;
import process.recognition.shape.ShapeRecognizerListener;
import process.recognition.shape.TriangleRecognition;
import utils.AppLogger;
import utils.FileUtilities;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */
public class ImageProcessingImpl implements ImageProcessing {

    private ImageProcessingCallback callback;
    private final RGBtoNHS rgbToNHS;
    private final ImageDataStructure imageDataStructure;
    private MarchingSquares marchingSquares;
    private long startTime;

    /**
     * Constructor.
     */
    public ImageProcessingImpl() {
        imageDataStructure = new ImageDataStructure();
        rgbToNHS = new RGBtoNHS();
    }

    @Override
    public void read(BufferedImage sourceImage) throws IOException {

        final int sourceImageWidth = sourceImage.getWidth();
        final int sourceImageHeight = sourceImage.getHeight();

        if (!imageDataStructure.isInit()) {
            imageDataStructure.init(sourceImageWidth * sourceImageHeight);
        }

        // Scale large images
        //if (sourceImageWidth > SOURCE_IMG_WIDTH_LIMIT && sourceImageHeight > SOURCE_IMG_HEIGHT_LIMIT) {
            //sourceImage = ImageUtils.resizeImage(sourceImage, SOURCE_IMG_WIDTH_LIMIT, SOURCE_IMG_HEIGHT_LIMIT,
            //        BufferedImage.TYPE_INT_RGB);
        //}

        if (callback != null) {
            callback.onSourceImageSetup(sourceImage);
        }

        final BufferedImage imageDifference = new BufferedImage(sourceImageWidth, sourceImageHeight,
                BufferedImage.TYPE_INT_RGB);

        imageDataStructure.reset();

        startTime = System.currentTimeMillis();

        rgbToNHS.convert(
                sourceImage.getRGB(0, 0, sourceImageWidth, sourceImageHeight, null, 0, sourceImageWidth),
                imageDataStructure
        );

        //AppLogger.printMessage("Convert time:" + (System.currentTimeMillis() - startTime) + " ms");

        marchingSquares = new MarchingSquares(sourceImageWidth, sourceImageHeight,
                imageDataStructure.getNhsRedAsArray());

        final java.util.List<Path> paths = marchingSquares.identifyPerimeter();

        final Graphics2D graphics2D = imageDifference.createGraphics();
        graphics2D.setColor(Color.RED);

        Path path;

        // Eliminate small objects
        int minWidth = sourceImageWidth * 3 / 100;
        int minHeight = sourceImageHeight * 3 / 100;
        for (int j = 0; j < paths.size(); j++) {
            path = paths.get(j);
            //AppLogger.printMessage("Shape w:" + path.getShapeWidth() + " " + (imageDifference.getWidth()));
            if (path.getShapeWidth() < minWidth || path.getShapeHeight() < minHeight) {
                paths.remove(path);
                j--;
            }
        }

        // Eliminate nested shapes
        Path path_I;
        for (int j = 0; j < paths.size(); j++) {

            path = paths.get(j);
            for (int i = j + 1; i < paths.size(); i++) {

                path_I = paths.get(i);
                if (path.getMinX() <= path_I.getMinX() && path.getMinY() <= path_I.getMinY() &&
                        path.getMaxX() >= path_I.getMaxX() && path.getMaxY() >= path_I.getMaxY()) {

                    paths.remove(path_I);
                    //j++;
                    i--;
                }
            }
        }

        // Recognize shapes
        final EllipseRecognition ellipseRecognition = new EllipseRecognition();
        final TriangleRecognition triangleRecognition = new TriangleRecognition();

        final ShapeRecognizerListener compositeRecognizer = new CompositeShapeRecognizer(ellipseRecognition,
                triangleRecognition);

        Direction direction;
        int lastX;
        int lastY;

        //AppLogger.printMessage("Shapes N:" + paths.size());

        for (Path pathFinal : paths) {

            compositeRecognizer.recognizeShape(pathFinal, new ImageRecognizingCallback() {

                @Override
                public void onRecognize(RecognizedShape recognizedShape) {
                    callback.onRecognize(recognizedShape);
                }
            });

            lastX = pathFinal.getOriginX();
            lastY = -pathFinal.getOriginY();

            //graphics2D.drawRect(lastX, lastY, 1, 1);

            for (int i = 0; i < pathFinal.getLength(); i++) {

                direction = pathFinal.getDirections().get(i);

                lastX += direction.screenX;
                lastY += direction.screenY;

                if (i % 10 == 0) {
                    graphics2D.drawRect(lastX, lastY, 1, 1);
                }
                //graphics2D.drawRect(lastX, lastY, 1, 1);
            }

            //break;
        }

        //distance(imageDataStructure);

        //drawDifferences(imageDataStructure, imageDifference);

        if (callback != null) {
            callback.onComplete(imageDifference);
        }
    }

    @Override
    public void read(String sourceImagePath) throws Exception {
        if (sourceImagePath == null) {
            AppLogger.printError(getClass().getSimpleName() + " read() method, Source Image URL must not be null");
            throw new NullPointerException();
        }
        if (sourceImagePath.equals("")) {
            AppLogger.printError(getClass().getSimpleName() + " read() method, Source Image URL must not be empty");
            throw new Exception();
        }

        read(FileUtilities.loadImage(sourceImagePath));
    }

    @Override
    public void read(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            AppLogger.printError(getClass().getSimpleName() + " read() method, Source Image stream must not be null");
            throw new NullPointerException();
        }
        read(FileUtilities.loadImage(inputStream));
    }

    public void setCallback(ImageProcessingCallback callback) {
        this.callback = callback;
    }

    private double getMotionVector(int pointA_X, int pointA_Y, int pointB_X, int pointB_Y) {
        double a = Math.abs(pointA_Y - pointB_Y);
        double b = Math.abs(pointA_X - pointB_X);
        if (b == 0) {
            b = 1;
        }
        if (a == 0) {
            a = 1;
        }
        double d = (b / a);
        //AppLogger.printMessage("a:" + a + " b:" + b + " d:" + d);
        return Math.toDegrees(Math.atan(d));
    }

    /**
     * http://ostermiller.org/dilate_and_erode.html
     *
     * O(n) solution to find the distance to "on" pixels in a single dimension array
     *
     * @param imageGraph {@value process.image.ImageDataStructure}
     */
    private void distance(ImageDataStructure imageGraph) {
        // traverse forwards
        int pixel;
        for (int i = 0; i < imageGraph.getVertexesSize(); i++) {
            pixel = imageGraph.getVertexAt(i).getNhsRed();
            if (pixel == 255) {
                // first pass and pixel was on, it gets a zero
                //arr[i] = 0;
                imageGraph.getVertexAt(i).setNhsRed(0);
            } else {
                // pixel was off
                // It is at most the length of array
                // away from a pixel that is on

                imageGraph.getVertexAt(i).setNhsRed(imageGraph.getVertexesSize());

                // One more than the pixel to the left
                if (i > 0) {
                    imageGraph.getVertexAt(i).setNhsRed(Math.min(imageGraph.getVertexAt(i).getNhsRed(),
                                                        imageGraph.getVertexAt(i - 1).getNhsRed() + 255));
                }
            }
        }
        // traverse backwards
        for (int i = imageGraph.getVertexesSize() - 1; i >= 0; i--) {
            // what we had on the first pass
            // or one more than the pixel to the right
            if (i + 255 < imageGraph.getVertexesSize()) {
                imageGraph.getVertexAt(i).setNhsRed(Math.min(imageGraph.getVertexAt(i).getNhsRed(),
                                                    imageGraph.getVertexAt(i + 1).getNhsRed() + 255));
            }
        }
    }

    private void drawDifferences(ImageDataStructure graph, BufferedImage imageDifference) {
        //AppLogger.printMessage("Start draw differences ...");

        ImageVertex vertex;
        int pixel;
        for (int i = 0; i < graph.getVertexesSize(); i++) {
            vertex = graph.getVertexAt(i);
            pixel = vertex.getNhsRed();
            // for ARGB
            //pixel = (0xff << 24) + (pixel << 16) + (pixel << 8) + pixel;
            //pixel = (pixel << 16) + (pixel << 8) + pixel;

            if (pixel == 255) {
                //AppLogger.printMessage("pixel:" + pixel);
                pixel = 0xffffff;
            } else {
                pixel = 0;
            }

            imageDifference.setRGB(vertex.getXPosition(), vertex.getYPosition(), pixel);
        }

        //AppLogger.printMessage("Draw differences complete");
    }
}