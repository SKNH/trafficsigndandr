package process;

import ocr.ocrPlugins.mseOCR.OCRScanner;
import process.data.DataInitializer;
import process.recognition.CompositeSpeedRecognizer;
import process.recognition.RecognizedShape;
import process.recognition.SpeedRecognition;
import utils.AppLogger;
import utils.FileUtilities;
import utils.ImageUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:56 AM
 */
public class ImageProcessingCallbackImpl implements ImageProcessingCallback {

    private OCRScanner ocrScanner;
    private int subImagesCounter;

    public ImageProcessingCallbackImpl(OCRScanner ocrScanner) {
        this.ocrScanner = ocrScanner;
        FileUtilities.deleteDifferenceImage();
    }

    private BufferedImage sourceImage;

    @Override
    public void onSourceImageSetup(BufferedImage sourceImage) {

        FileUtilities.deleteSubImages();

        this.sourceImage = sourceImage;
        subImagesCounter = 0;
    }

    @Override
    public void onComplete(BufferedImage resultImage) throws IOException {
        AppLogger.printMessage("Processing complete");
        if (resultImage != null) {

            FileUtilities.createDirIfNeeded("output");

            File outputFile = new File("output/image_difference.png");
            ImageIO.write(resultImage, "png", outputFile);
        } else {
            AppLogger.printMessage("There are no differences between images");
        }
    }

    @Override
    public void onComplete(byte[] resultImage) {

    }

    @Override
    public void onRecognize(final RecognizedShape recognizedShape) {
        AppLogger.printMessage(recognizedShape.getShape() + " detected");

        BufferedImage subImage = sourceImage.getSubimage(recognizedShape.getMinX(), recognizedShape.getMinY(),
                (recognizedShape.getMaxX() - recognizedShape.getMinX()),
                (recognizedShape.getMaxY() - recognizedShape.getMinY()));

        FileUtilities.createDirIfNeeded("output");

        subImage = ImageUtils.resizeImage(
                subImage, DataInitializer.BASE_IMAGE_WIDTH,
                DataInitializer.BASE_IMAGE_HEIGHT, BufferedImage.TYPE_BYTE_GRAY
        );

        final Graphics2D graphics2D = subImage.createGraphics();
        graphics2D.setColor(Color.RED);

        SpeedRecognition speedRecognition = null;

        if (recognizedShape.getShape() == RecognizedShape.SHAPE.ELLIPSE) {
            speedRecognition = new SpeedRecognition();
        }

        final CompositeSpeedRecognizer compositeSpeedRecognizer = new CompositeSpeedRecognizer(speedRecognition);

        final int[] recognizedData = new int[subImage.getWidth() * subImage.getHeight()];
        subImage.getRGB(
                0, 0, subImage.getWidth(), subImage.getHeight(), recognizedData, 0, subImage.getWidth()
        );

        compositeSpeedRecognizer.recognizeShape(
                ocrScanner, recognizedData, subImage.getWidth(), subImage.getHeight(), callback
        );

        final File outputFile = new File("output/sub_image_" + (subImagesCounter++) + ".jpg");
        try {
            ImageIO.write(subImage, "jpg", outputFile);
        } catch (IOException e) {
            AppLogger.printError("Can not draw sub image: " + e.getMessage());
        }
    }

    private SpeedRecognition.ISpeedRecognitionCallback callback = new SpeedRecognition.ISpeedRecognitionCallback() {

        @Override
        public void onComplete(String message) {
            AppLogger.printMessage("Scan result:" + message);
        }
    };
}
