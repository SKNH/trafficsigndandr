package process.data;

import ocr.ocrPlugins.mseOCR.CharacterRange;
import ocr.ocrPlugins.mseOCR.OCRScanner;
import ocr.ocrPlugins.mseOCR.TrainingImage;
import ocr.ocrPlugins.mseOCR.TrainingImageLoader;
import utils.AppLogger;
import utils.ImageUtils;
import utils.TabularDictionary;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 4/9/14
 * Time: 2:53 PM
 */
public class DataInitializer {

    /**
     * Interface to provide {@link process.data.DataInitializer} lifecycle events
     */
    public interface IDataInitializer {

        /**
         * Call when initialization complete
         * @throws Exception
         */
        public void onInitializationComplete() throws Exception;

        /**
         * Call when an error occurs while initializing
         * @param message error message
         */
        public void onInitializationError(String message);
    }

    /**
     * Width and Height of the base image which is used to interpolate with detected sign
     */
    public static final int BASE_IMAGE_WIDTH = 50;
    public static final int BASE_IMAGE_HEIGHT = 50;

    private final OCRScanner ocrScanner = new OCRScanner();

    private IDataInitializer callback;

    public DataInitializer() {

    }

    public void setCallback(IDataInitializer callback) {
        this.callback = callback;
    }

    public void init() throws Exception {

        if (callback == null) {
            throw new NullPointerException(DataInitializer.class.getSimpleName() + " callback must specified" +
                    " before initializing");
        }

        // Initialize tabular data
        TabularDictionary.init();

        // Train OCRScanner
        trainingOCR();

        callback.onInitializationComplete();
    }

    public OCRScanner getOcrScanner() {
        return ocrScanner;
    }

    /**
     * Load demo training images.
     */
    private void trainingOCR() {

        ocrScanner.clearTrainingImages();
        final TrainingImageLoader trainingImageLoader = new TrainingImageLoader();
        final HashMap<Character, ArrayList<TrainingImage>> trainingImageMap = new HashMap<>();
        try {
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v1.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v2.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v3.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v4.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v5.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v6.jpg"),
                    new CharacterRange('0'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_0_v7.jpg"),
                    new CharacterRange('0'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_1_v1.jpg"),
                    new CharacterRange('1'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_1_v2.jpg"),
                    new CharacterRange('1'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_2_v1.jpg"),
                    new CharacterRange('2'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_2_v2.jpg"),
                    new CharacterRange('2'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v1.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v2.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v3.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v4.jpg"),
                    new CharacterRange('3'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_3_v5.jpg"),
                    new CharacterRange('3'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_4_v1.jpg"),
                    new CharacterRange('4'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_4_v2.jpg"),
                    new CharacterRange('4'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_4_v3.jpg"),
                    new CharacterRange('4'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v1.jpg"),
                    new CharacterRange('5'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v2.jpg"),
                    new CharacterRange('5'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v3.jpg"),
                    new CharacterRange('5'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_5_v4.jpg"),
                    new CharacterRange('5'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_6_v1.jpg"),
                    new CharacterRange('6'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_6_v2.jpg"),
                    new CharacterRange('6'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_6_v3.jpg"),
                    new CharacterRange('6'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_7_v1.jpg"),
                    new CharacterRange('7'), trainingImageMap);
            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_7_v2.jpg"),
                    new CharacterRange('7'), trainingImageMap);

            trainingImageLoader.load(
                    ImageUtils.resourceImageToPixelImage("digit_9_v1.jpg"),
                    new CharacterRange('9'), trainingImageMap);

            /*ocrScanner.acceptAccuracyListener(new AccuracyListener() {
                @Override
                public void processCharOrSpace(OCRIdentification ocrIdentification) {
                    //AppLogger.printMessage("Listener:" + ocrIdentification.toString());
                }
            });*/

            ocrScanner.addTrainingImages(trainingImageMap);
            AppLogger.printMessage("OCRScanner trained.");
        } catch (IOException ex) {
            callback.onInitializationError(DataInitializer.class.getSimpleName() + " OCR training error:" +
                    ex.getMessage());
        }
    }
}