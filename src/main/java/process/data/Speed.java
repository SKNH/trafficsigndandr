package process.data;

import utils.AppLogger;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/9/14
 * Time: 8:47 PM
 */
public class Speed {

    public static final int UNDEFINED = -1;
    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int THREE = 3;
    public static final int FIVE = 5;
    public static final int TEN = 10;
    public static final int FIFTEEN = 15;
    public static final int TWENTY = 20;
    public static final int THIRTY = 30;
    public static final int FORTY = 40;
    public static final int FIFTY = 50;
    public static final int SIXTY = 60;
    public static final int SEVENTY = 70;
    public static final int EIGHTY = 80;
    public static final int NINETY = 90;
    public static final int HUNDRED = 100;
    public static final int HUNDRED_TEN = 110;
    public static final int HUNDRED_TWENTY = 120;
    public static final int HUNDRED_THIRTY = 130;

    public static final int length = 19;

    public static enum SPEED {
        ZERO, ONE, THREE, FIVE, TEN, FIFTEEN, TWENTY, THIRTY, FORTY, FIFTY, SIXTY, SEVENTY, EIGHTY, NINETY, HUNDRED, HUNDRED_TEN,
        HUNDRED_TWENTY, HUNDRED_THIRTY
    }

    public static int valueToInt(SPEED speed) {
        switch (speed) {
            case ZERO:
                return ZERO;
            case ONE:
                return ONE;
            case THREE:
                return THREE;
            case FIVE:
                return FIVE;
            case TEN:
                return TEN;
            case FIFTEEN:
                return FIFTEEN;
            case TWENTY:
                return TWENTY;
            case THIRTY:
                return THIRTY;
            case FORTY:
                return FORTY;
            case FIFTY:
                return FIFTY;
            case SIXTY:
                return SIXTY;
            case SEVENTY:
                return SEVENTY;
            case EIGHTY:
                return EIGHTY;
            case NINETY:
                return NINETY;
            case HUNDRED:
                return HUNDRED;
            case HUNDRED_TEN:
                return HUNDRED_TEN;
            case HUNDRED_TWENTY:
                return HUNDRED_TWENTY;
            case HUNDRED_THIRTY:
                return HUNDRED_THIRTY;
            default:
                AppLogger.printError("Unknown value " + speed + " of the speed");
                return UNDEFINED;
        }
    }
}