package process;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:54 AM
 */
public interface ImageProcessingCallback extends ImageProcessingCallbackBase {

    /**
     * This function is call when processing of the image is complete
     * @param resultImage {@link java.awt.image.BufferedImage} as result
     * @throws java.io.IOException
     */
    public void onComplete(BufferedImage resultImage) throws IOException;

    /**
     * This function is call when processing of the image is complete
     * @param resultImage array of the bytes as result
     */
    public void onComplete(byte[] resultImage);

    /**
     * This function is call when source image has been set to process
     * @param sourceImage source image
     */
    public void onSourceImageSetup(BufferedImage sourceImage);
}