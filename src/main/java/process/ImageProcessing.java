package process;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/17/13
 * Time: 9:49 AM
 */
public interface ImageProcessing {

    public void read(BufferedImage sourceImage) throws IOException;

    public void read(String sourceImagePath) throws Exception;

    public void read(InputStream inputStream) throws Exception;
}