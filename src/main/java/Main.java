import com.googlecode.javacv.FrameGrabber;
import images.ResourcesImagesLoader;
import ocr.ocrPlugins.mseOCR.OCRScanner;
import process.ImageProcessingCallbackImpl;
import process.ImageProcessingImpl;
import process.data.DataInitializer;
import utils.AppLogger;
import utils.AppUtils;
import view.CameraCapture;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main {

    public static void main(String[] args) {
        AppLogger.printMessage("Traffic Signs Detection and Recognition");

        new Main();
    }

    public Main() {
        try {
            initializeData();
        } catch (Exception e) {
            e.printStackTrace();
            AppLogger.printError("Image processing error:" + e);
        }

        //processCamera();
    }

    private void initializeData() throws Exception {

        //InitData initData = new InitData();
        final DataInitializer dataInitializer = new DataInitializer();
        dataInitializer.setCallback(new DataInitializer.IDataInitializer() {

            @Override
            public void onInitializationComplete() throws Exception {
                doProcess(dataInitializer.getOcrScanner());
            }

            @Override
            public void onInitializationError(String message) {
                AppLogger.printError(message);
            }
        });

        dataInitializer.init();
    }

    private void doProcess(OCRScanner ocrScanner) throws Exception {
        // circular0008 teamp deer_road_sign give_way_01;
        String sourceImagePath;
        sourceImagePath = "speed_limit_06.jpg";
        //sourceImagePath = "circle_24.jpg";
        //sourceImagePath = "speed_limit_origin_50_ukr.gif";
        //sourceImagePath = "UKMaximumSpeedLimitSigns.png";

        final ImageProcessingCallbackImpl imageProcessingCallback = new ImageProcessingCallbackImpl(ocrScanner);

        final ImageProcessingImpl imageProcessing = new ImageProcessingImpl();
        imageProcessing.setCallback(imageProcessingCallback);
        for (int i = 0; i < 10; i++) {
            imageProcessing.read(ResourcesImagesLoader.class.getResourceAsStream(sourceImagePath));
        }
    }

    private void processCamera() {

        final CameraCapture cameraCapture = new CameraCapture();
        try {
            cameraCapture.start();
        } catch (FrameGrabber.Exception e) {
            AppLogger.printError("Process camera FrameGrabber.Exception:" + e.getMessage());
        }

        Frame frame = new Frame("Camera View");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    cameraCapture.stop();
                } catch (FrameGrabber.Exception e1) {
                    AppLogger.printError("Process camera stop:" + e1.getMessage());
                }
                System.exit(0);
            }
        });
        frame.add("Center", cameraCapture);
        frame.pack();
        frame.setSize(new Dimension(AppUtils.MAIN_PANEL_WIDTH, AppUtils.MAIN_PANEL_HEIGHT));
        frame.setVisible(true);
    }
}