package utils;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 21.10.14
 * Time: 21:16
 */
public class AppUtils {

    /**
     * Dimensions
     */
    public static final int MAIN_PANEL_WIDTH = 640;
    public static final int MAIN_PANEL_HEIGHT = 640;
    public static final int CAMERA_WIDTH = 640;
    public static final int CAMERA_HEIGHT = 480;
}