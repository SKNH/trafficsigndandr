package utils;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 4/9/14
 * Time: 5:15 PM
 */
public class BytesUtils {

    public static void dumpInteger(int value) {
        AppLogger.printMessage("1 byte:" + (value & 0x000000FF));
        AppLogger.printMessage("2 byte:" + ((value & 0x0000FF00) >> 8));
        AppLogger.printMessage("3 byte:" + ((value & 0x00FF0000) >> 16));
        AppLogger.printMessage("4 byte:" + ((value & 0xFF000000) >> 24));
    }
}