package utils;

import images.ResourcesImagesLoader;
import ocr.scanner.PixelImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 4/13/14
 * Time: 9:41 PM
 */
public final class ImageUtils {

    /**
     * Private constructor
     */
    private ImageUtils() {

    }

    /**
     * Resize an image to provided dimensions
     *
     * @param originalImage  original image
     * @param resizeToWidth  width value resize to
     * @param resizeToHeight height value to resize to
     * @param type           type of the image
     * @return scaled image
     */
    public static BufferedImage resizeImage(BufferedImage originalImage, int resizeToWidth, int resizeToHeight,
                                            int type) {
        double originalWidth = originalImage.getWidth();
        double originalHeight = originalImage.getHeight();
        double widthRatio = originalWidth / originalHeight;
        double heightRatio = originalHeight / originalWidth;
        if (originalWidth >= originalHeight) {
            resizeToHeight = (int) (resizeToWidth * heightRatio);
        } else {
            resizeToWidth = (int) (resizeToHeight * widthRatio);
        }

        //AppLogger.printMessage("Resize image to w:" + resizeToWidth + " h:" + resizeToHeight +
        //        " wRatio:" + widthRatio + " hRatio:" + heightRatio);

        BufferedImage resizedImage = new BufferedImage(resizeToWidth, resizeToHeight, type);
        Graphics2D graphics = resizedImage.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics.drawImage(originalImage, 0, 0, resizeToWidth, resizeToHeight, null);
        graphics.dispose();
        return resizedImage;
    }

    /**
     * Load an Image from the resources and transform it into the {@link ocr.scanner.PixelImage}.
     *
     * @param imagePath Path to the image file in the resources
     * @return {@link ocr.scanner.PixelImage}
     * @throws IOException
     */
    public static PixelImage resourceImageToPixelImage(final String imagePath) throws IOException {
        final InputStream inputStream = ResourcesImagesLoader.class.getResourceAsStream(imagePath);
        final BufferedImage bufferedImage = ImageIO.read(inputStream);
        final int[] pixelsArray = new int[bufferedImage.getWidth() * bufferedImage.getHeight()];
        bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), pixelsArray,
                0, bufferedImage.getWidth());
        return new PixelImage(pixelsArray, bufferedImage.getWidth(), bufferedImage.getHeight());
    }
}