package utils;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/16/13
 * Time: 9:09 PM
 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class contains help methods to process File
 */
public class FileUtilities {

    /**
     * Load Image from file.
     *
     * @param fileName Name of the file.
     *
     * @return {@link java.awt.image.BufferedImage}
     *
     * @throws java.io.IOException
     */
    public static BufferedImage loadImage(String fileName) throws IOException {
        return ImageIO.read(new File(fileName));
    }

    /**
     * Load Image from input stream.
     *
     * @param inputStream {@link java.io.InputStream} of the image file
     *
     * @return {@link java.awt.image.BufferedImage}
     *
     * @throws IOException
     */
    public static BufferedImage loadImage(InputStream inputStream) throws IOException {
        return ImageIO.read(inputStream);
    }

    /**
     * This method creates a directory with given name is such does not exists
     *
     * @param path a path to the directory
     */
    public static void createDirIfNeeded(String path) {
        File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static void deleteDifferenceImage() {
        File outputFile = new File("output/image_difference.png");
        if (outputFile.exists()) {
            outputFile.delete();
        }
    }

    public static void deleteSubImages() {
        File outputDir = new File("output");
        if (!outputDir.isDirectory()) {
            return;
        }

        FilenameFilter subImageFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                return lowercaseName.startsWith("sub_image");
            }
        };

        File[] files = outputDir.listFiles(subImageFilter);
        for (File outputFile : files) {
            if (outputFile.exists()) {
                outputFile.delete();
            }
        }
    }
}